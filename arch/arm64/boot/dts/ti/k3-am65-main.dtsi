// SPDX-License-Identifier: GPL-2.0
/*
 * Device Tree Source for AM6 SoC Family Main Domain peripherals
 *
 * Copyright (C) 2016-2018 Texas Instruments Incorporated - http://www.ti.com/
 */

&cbass_main {
	gic500: interrupt-controller@1800000 {
		compatible = "arm,gic-v3";
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;
		#interrupt-cells = <3>;
		interrupt-controller;
		reg = <0x00 0x01800000 0x00 0x10000>,	/* GICD */
		      <0x00 0x01880000 0x00 0x90000>;	/* GICR */
		/*
		 * vcpumntirq:
		 * virtual CPU interface maintenance interrupt
		 */
		interrupts = <GIC_PPI 9 IRQ_TYPE_LEVEL_HIGH>;

		gic_its: gic-its@18200000 {
			compatible = "arm,gic-v3-its";
			reg = <0x00 0x01820000 0x00 0x10000>;
			msi-controller;
			#msi-cells = <1>;
		};
	};

	secure_proxy_main: mailbox@32c00000 {
		compatible = "ti,am654-secure-proxy";
		#mbox-cells = <1>;
		reg-names = "target_data", "rt", "scfg";
		reg = <0x00 0x32c00000 0x00 0x100000>,
		      <0x00 0x32400000 0x00 0x100000>,
		      <0x00 0x32800000 0x00 0x100000>;
		interrupt-names = "rx_011";
		interrupts = <GIC_SPI 37 IRQ_TYPE_LEVEL_HIGH>;
	};

	main_uart0: serial@2800000 {
		compatible = "ti,am654-uart";
		reg = <0x00 0x02800000 0x00 0x100>;
		reg-shift = <2>;
		reg-io-width = <4>;
		interrupts = <GIC_SPI 192 IRQ_TYPE_LEVEL_HIGH>;
		clock-frequency = <48000000>;
		current-speed = <115200>;
	};

	main_uart1: serial@2810000 {
		compatible = "ti,am654-uart";
		reg = <0x00 0x02810000 0x00 0x100>;
		reg-shift = <2>;
		reg-io-width = <4>;
		interrupts = <GIC_SPI 193 IRQ_TYPE_LEVEL_HIGH>;
		clock-frequency = <48000000>;
		current-speed = <115200>;
	};

	main_uart2: serial@2820000 {
		compatible = "ti,am654-uart";
		reg = <0x00 0x02820000 0x00 0x100>;
		reg-shift = <2>;
		reg-io-width = <4>;
		interrupts = <GIC_SPI 194 IRQ_TYPE_LEVEL_HIGH>;
		clock-frequency = <48000000>;
		current-speed = <115200>;
	};

	main_intr: interrupt-controller0 {
		compatible = "ti,sci-intr";
		interrupt-controller;
		interrupt-parent = <&gic500>;
		#interrupt-cells = <3>;
		ti,sci = <&dmsc>;
		ti,sci-dst-id = <56>;
		ti,sci-rm-range-girq = <0x1>;
	};

	main_navss: main_navss {
		compatible = "simple-bus";
		#address-cells = <2>;
		#size-cells = <2>;
		dma-coherent;
		dma-ranges;
		ranges;

		ti,sci-dev-id = <118>;

		main_navss_intr: interrupt-controller1 {
			compatible = "ti,sci-intr";
			interrupt-controller;
			interrupt-parent = <&gic500>;
			#interrupt-cells = <3>;
			ti,sci = <&dmsc>;
			ti,sci-dst-id = <56>;
			ti,sci-rm-range-girq = <0x0>,
					       <0x2>;
		};

		main_udmass_inta: interrupt-controller@33d00000 {
			compatible = "ti,sci-inta";
			reg = <0x0 0x33d00000 0x0 0x100000>;
			interrupt-controller;
			interrupt-parent = <&main_navss_intr>;
			#interrupt-cells = <3>;
			ti,sci = <&dmsc>;
			ti,sci-dev-id = <179>;
			ti,sci-rm-range-vint = <0x0>;
			ti,sci-rm-range-global-event = <0x1>;
		};

		hwspinlock: spinlock@30e00000 {
			compatible = "ti,am654-hwspinlock";
			reg = <0x00 0x30e00000 0x00 0x1000>;
			#hwlock-cells = <1>;
		};

		mailbox0_cluster0: mailbox@31f80000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f80000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			interrupt-parent = <&main_navss_intr>;
			interrupts = <164 0 IRQ_TYPE_LEVEL_HIGH>;

			mbox_mcu_r5fss0_core0: mbox-mcu-r5fss0-core0 {
				ti,mbox-tx = <1 0 0>;
				ti,mbox-rx = <0 0 0>;
			};
		};

		mailbox0_cluster1: mailbox@31f81000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f81000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			interrupt-parent = <&main_navss_intr>;
			interrupts = <165 0 IRQ_TYPE_LEVEL_HIGH>;

			mbox_mcu_r5fss0_core1: mbox-mcu-r5fss0-core1 {
				ti,mbox-tx = <1 0 0>;
				ti,mbox-rx = <0 0 0>;
			};
		};

		mailbox0_cluster2: mailbox@31f82000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f82000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster3: mailbox@31f83000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f83000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster4: mailbox@31f84000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f84000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster5: mailbox@31f85000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f85000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster6: mailbox@31f86000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f86000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster7: mailbox@31f87000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f87000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster8: mailbox@31f88000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f88000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster9: mailbox@31f89000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f89000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster10: mailbox@31f8a000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f8a000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		mailbox0_cluster11: mailbox@31f8b000 {
			compatible = "ti,am654-mailbox";
			reg = <0x00 0x31f8b000 0x00 0x200>;
			#mbox-cells = <1>;
			ti,mbox-num-users = <4>;
			ti,mbox-num-fifos = <16>;
			status = "disabled";
		};

		ringacc: ringacc@3c000000 {
			compatible = "ti,am654-navss-ringacc";
			reg =	<0x0 0x3c000000 0x0 0x400000>,
				<0x0 0x38000000 0x0 0x400000>,
				<0x0 0x31120000 0x0 0x100>,
				<0x0 0x33000000 0x0 0x40000>;
			reg-names = "rt", "fifos", "proxy_gcfg", "proxy_target";
			ti,num-rings = <818>;
			ti,gp-rings = <304 464>; /* start, cnt */
			ti,dma-ring-reset-quirk;
			ti,sci = <&dmsc>;
			ti,sci-dev-id = <187>;
			interrupt-parent = <&main_udmass_inta>;
		};

		main_udmap: udmap@31150000 {
			compatible = "ti,am654-navss-main-udmap";
			reg =	<0x0 0x31150000 0x0 0x100>,
				<0x0 0x34000000 0x0 0x100000>,
				<0x0 0x35000000 0x0 0x100000>;
			reg-names = "gcfg", "rchanrt", "tchanrt";
			#dma-cells = <3>;

			ti,ringacc = <&ringacc>;
			ti,psil-base = <0x1000>;

			interrupt-parent = <&main_udmass_inta>;

			ti,sci = <&dmsc>;
			ti,sci-dev-id = <188>;

			ti,sci-rm-range-tchan = <0x1>, /* TX_HCHAN */
						<0x2>; /* TX_CHAN */
			ti,sci-rm-range-rchan = <0x4>, /* RX_HCHAN */
						<0x5>; /* RX_CHAN */
			ti,sci-rm-range-rflow = <0x6>; /* GP RFLOW */
		};
	};

	pdma0: pdma@2a41000 {
		compatible = "ti,am654-pdma";
		reg = <0x0 0x02A41000 0x0 0x400>;
		reg-names = "eccaggr_cfg";

		ti,psil-base = <0x4400>;

		/* ti,psil-config0-2 */
		UDMA_PDMA_TR_XY(0);
		UDMA_PDMA_TR_XY(1);
		UDMA_PDMA_TR_XY(2);
	};

	pdma1: pdma@2a42000 {
		compatible = "ti,am654-pdma";
		reg = <0x0 0x02A42000 0x0 0x400>;
		reg-names = "eccaggr_cfg";

		ti,psil-base = <0x4500>;

		/* ti,psil-config0-22 */
		UDMA_PDMA_TR_XY(0);
		UDMA_PDMA_TR_XY(1);
		UDMA_PDMA_TR_XY(2);
		UDMA_PDMA_TR_XY(3);
		UDMA_PDMA_TR_XY(4);
		UDMA_PDMA_TR_XY(5);
		UDMA_PDMA_TR_XY(6);
		UDMA_PDMA_TR_XY(7);
		UDMA_PDMA_TR_XY(8);
		UDMA_PDMA_TR_XY(9);
		UDMA_PDMA_TR_XY(10);
		UDMA_PDMA_TR_XY(11);
		UDMA_PDMA_TR_XY(12);
		UDMA_PDMA_TR_XY(13);
		UDMA_PDMA_TR_XY(14);
		UDMA_PDMA_TR_XY(15);
		UDMA_PDMA_TR_XY(16);
		UDMA_PDMA_TR_XY(17);
		UDMA_PDMA_TR_XY(18);
		UDMA_PDMA_TR_XY(19);
		UDMA_PDMA_PKT_XY(20);
		UDMA_PDMA_PKT_XY(21);
		UDMA_PDMA_PKT_XY(22);
	};

	eip76d_trng: trng@4e10000 {
		compatible = "inside-secure,safexcel-eip76";
		reg = <0x0 0x4e10000 0x0 0x7d>;
		interrupts = <GIC_SPI 24 IRQ_TYPE_LEVEL_HIGH>;
		clocks = <&k3_clks 136 1>;
	};

	crypto: crypto@4E00000 {
		compatible = "ti,sa2ul-crypto";
		label = "crypto-aes-gbe";
		reg = <0x0 0x4E00000 0x0 0x1200>;

		status = "okay";
		ti,psil-base = <0x4000>;

		/* tx: crypto_pnp-1, rx: crypto_pnp-1 */
		dmas = <&main_udmap &crypto 0 UDMA_DIR_TX>,
				<&main_udmap &crypto 0 UDMA_DIR_RX>,
				<&main_udmap &crypto 1 UDMA_DIR_RX>;
		dma-names = "tx", "rx1", "rx2";

		ti,psil-config0 {
			linux,udma-mode = <UDMA_PKT_MODE>;
			ti,needs-epib;
			ti,psd-size = <64>;
		};

		ti,psil-config1 {
			linux,udma-mode = <UDMA_PKT_MODE>;
			ti,needs-epib;
			ti,psd-size = <64>;
		};

		ti,psil-config2 {
			linux,udma-mode = <UDMA_PKT_MODE>;
			ti,needs-epib;
			ti,psd-size = <64>;
		};
	};

	main_pmx0: pinmux@11c000 {
		compatible = "pinctrl-single";
		reg = <0x0 0x11c000 0x0 0x2e4>;
		#pinctrl-cells = <1>;
		pinctrl-single,register-width = <32>;
		pinctrl-single,function-mask = <0xffffffff>;
	};

	main_pmx1: pinmux@11c2e8 {
		compatible = "pinctrl-single";
		reg = <0x0 0x11c2e8 0x0 0x24>;
		#pinctrl-cells = <1>;
		pinctrl-single,register-width = <32>;
		pinctrl-single,function-mask = <0xffffffff>;
	};

	main_i2c0: i2c@2000000 {
		compatible = "ti,am654-i2c", "ti,omap4-i2c";
		reg = <0x0 0x2000000 0x0 0x100>;
		interrupts = <GIC_SPI 200 IRQ_TYPE_LEVEL_HIGH>;
		#address-cells = <1>;
		#size-cells = <0>;
		clock-names = "fck";
		clocks = <&k3_clks 110 1>;
		power-domains = <&k3_pds 110>;
	};

	main_i2c1: i2c@2010000 {
		compatible = "ti,am654-i2c", "ti,omap4-i2c";
		reg = <0x0 0x2010000 0x0 0x100>;
		interrupts = <GIC_SPI 201 IRQ_TYPE_LEVEL_HIGH>;
		#address-cells = <1>;
		#size-cells = <0>;
		clock-names = "fck";
		clocks = <&k3_clks 111 1>;
		power-domains = <&k3_pds 111>;
	};

	main_i2c2: i2c@2020000 {
		compatible = "ti,am654-i2c", "ti,omap4-i2c";
		reg = <0x0 0x2020000 0x0 0x100>;
		interrupts = <GIC_SPI 202 IRQ_TYPE_LEVEL_HIGH>;
		#address-cells = <1>;
		#size-cells = <0>;
		clock-names = "fck";
		clocks = <&k3_clks 112 1>;
		power-domains = <&k3_pds 112>;
	};

	main_i2c3: i2c@2030000 {
		compatible = "ti,am654-i2c", "ti,omap4-i2c";
		reg = <0x0 0x2030000 0x0 0x100>;
		interrupts = <GIC_SPI 203 IRQ_TYPE_LEVEL_HIGH>;
		#address-cells = <1>;
		#size-cells = <0>;
		clock-names = "fck";
		clocks = <&k3_clks 113 1>;
		power-domains = <&k3_pds 113>;
	};

	main_gpio0:  main_gpio0@600000 {
		compatible = "ti,k2g-gpio", "ti,keystone-gpio";
		reg = <0x0 0x600000 0x0 0x100>;
		gpio-controller;
		#gpio-cells = <2>;
			interrupt-parent = <&main_intr>;
		interrupts = <57 256 IRQ_TYPE_EDGE_RISING>,
				<57 257 IRQ_TYPE_EDGE_RISING>,
				<57 258 IRQ_TYPE_EDGE_RISING>,
				<57 259 IRQ_TYPE_EDGE_RISING>,
				<57 260 IRQ_TYPE_EDGE_RISING>,
				<57 261 IRQ_TYPE_EDGE_RISING>;
		interrupt-controller;
		#interrupt-cells = <2>;
		ti,ngpio = <96>;
		ti,davinci-gpio-unbanked = <0>;
		clocks = <&k3_clks 57 0>;
		clock-names = "gpio";
	};

	main_gpio1:  main_gpio1@601000 {
		compatible = "ti,k2g-gpio", "ti,keystone-gpio";
		reg = <0x0 0x601000 0x0 0x100>;
		gpio-controller;
		#gpio-cells = <2>;
			interrupt-parent = <&main_intr>;
		interrupts = <58 256 IRQ_TYPE_EDGE_RISING>,
				<58 257 IRQ_TYPE_EDGE_RISING>,
				<58 258 IRQ_TYPE_EDGE_RISING>,
				<58 259 IRQ_TYPE_EDGE_RISING>,
				<58 260 IRQ_TYPE_EDGE_RISING>,
				<58 261 IRQ_TYPE_EDGE_RISING>;
		interrupt-controller;
			#interrupt-cells = <2>;
		ti,ngpio = <90>;
		ti,davinci-gpio-unbanked = <0>;
		clocks = <&k3_clks 58 0>;
		clock-names = "gpio";
	};

	icssg_soc_bus0: pruss-soc-bus@b026004 {
		compatible = "ti,am654-icssg-soc-bus";
		reg = <0x00 0x0b026004 0x00 0x4>;
		power-domains = <&k3_pds 62>;
		#address-cells = <1>;
		#size-cells = <1>;
		ranges = <0x0b000000 0x00 0x0b000000 0x100000>;
		dma-ranges;

		icssg0: icssg@b000000 {
			compatible = "ti,am654-icssg";
			reg = <0xb000000 0x80000>;
			interrupts = <GIC_SPI 254 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 255 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 256 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 257 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 258 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 259 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 260 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 261 IRQ_TYPE_LEVEL_HIGH>;
			interrupt-names = "host2", "host3", "host4",
					  "host5", "host6", "host7",
					  "host8", "host9";
			#address-cells = <1>;
			#size-cells = <1>;
			ranges;
			dma-ranges;

			icssg0_mem: memories@b000000 {
				reg = <0xb000000 0x2000>,
				      <0xb002000 0x2000>,
				      <0xb010000 0x10000>;
				reg-names = "dram0", "dram1",
					    "shrdram2";
			};

			icssg0_cfg: cfg@b026000 {
				compatible = "syscon";
				reg = <0xb026000 0x200>;
			};

			icssg0_coreclk_mux: coreclk_mux {
				#clock-cells = <0>;
				clocks = <&k3_clks 62 19>, /* icssg0_core_clk */
					 <&k3_clks 62 3>;  /* icssg0_iclk */
				assigned-clocks = <&icssg0_coreclk_mux>;
				assigned-clock-parents = <&k3_clks 62 3>;
			};

			icssg0_iepclk_mux: iepclk_mux {
				#clock-cells = <0>;
				clocks = <&k3_clks 62 10>,	/* icssg0_iep_clk */
					 <&icssg0_coreclk_mux>;	/* core_clk */
				assigned-clocks = <&icssg0_iepclk_mux>;
				assigned-clock-parents = <&icssg0_coreclk_mux>;
			};

			icssg0_iep0: iep@b02e000 {
				compatible = "syscon";
				reg = <0xb02e000 0x1000>;
				clocks = <&icssg0_iepclk_mux>;
			};

			icssg0_iep1: iep@b02f000 {
				compatible = "syscon";
				reg = <0xb02f000 0x1000>;
				clocks = <&icssg0_iepclk_mux>;
			};

			icssg0_mii_rt: mii-rt@b032000 {
				compatible = "syscon";
				reg = <0xb032000 0x100>;
			};

			icssg0_mii_g_rt: mii-g-rt@b033000 {
				compatible = "syscon";
				reg = <0xb033000 0x1000>;
			};

			icssg0_intc: interrupt-controller@b020000 {
				compatible = "ti,am654-icssg-intc";
				reg = <0xb020000 0x2000>;
				interrupt-controller;
				#interrupt-cells = <1>;
			};

			pru0_0: pru@b034000 {
				compatible = "ti,am654-pru";
				reg = <0xb034000 0x4000>,
				      <0xb022000 0x100>,
				      <0xb022400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-pru0_0-fw";
				interrupt-parent = <&icssg0_intc>;
				interrupts = <16>, <17>;
				interrupt-names = "vring", "kick";
			};

			rtu0_0: rtu@b004000 {
				compatible = "ti,am654-rtu";
				reg = <0xb004000 0x2000>,
				      <0xb023000 0x100>,
				      <0xb023400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-rtu0_0-fw";
				interrupt-parent = <&icssg0_intc>;
				interrupts = <20>, <21>;
				interrupt-names = "vring", "kick";
			};

			pru0_1: pru@b038000 {
				compatible = "ti,am654-pru";
				reg = <0xb038000 0x4000>,
				      <0xb024000 0x100>,
				      <0xb024400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-pru0_1-fw";
				interrupt-parent = <&icssg0_intc>;
				interrupts = <18>, <19>;
				interrupt-names = "vring", "kick";
			};

			rtu0_1: rtu@b006000 {
				compatible = "ti,am654-rtu";
				reg = <0xb006000 0x2000>,
				      <0xb023800 0x100>,
				      <0xb023c00 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-rtu0_1-fw";
				interrupt-parent = <&icssg0_intc>;
				interrupts = <22>, <23>;
				interrupt-names = "vring", "kick";
			};

			icssg0_mdio: mdio@b032400 {
				compatible = "ti,davinci_mdio";
				reg = <0xb032400 0x100>;
				clocks = <&k3_clks 62 3>;
				clock-names = "fck";
				#address-cells = <1>;
				#size-cells = <0>;
				bus_freq = <1000000>;
				status = "disabled";
			};
		};
	};

	icssg_soc_bus1: pruss-soc-bus@b126004 {
		compatible = "ti,am654-icssg-soc-bus";
		reg = <0x00 0x0b126004 0x00 0x4>;
		power-domains = <&k3_pds 63>;
		#address-cells = <1>;
		#size-cells = <1>;
		ranges = <0x0b100000 0x00 0x0b100000 0x100000>;
		dma-ranges;

		icssg1: icssg@b100000 {
			compatible = "ti,am654-icssg";
			reg = <0xb100000 0x80000>;
			interrupts = <GIC_SPI 262 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 263 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 264 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 265 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 266 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 267 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 268 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 269 IRQ_TYPE_LEVEL_HIGH>;
			interrupt-names = "host2", "host3", "host4",
					  "host5", "host6", "host7",
					  "host8", "host9";
			#address-cells = <1>;
			#size-cells = <1>;
			ranges;
			dma-ranges;

			icssg1_mem: memories@b100000 {
				reg = <0xb100000 0x2000>,
				      <0xb102000 0x2000>,
				      <0xb110000 0x10000>;
				reg-names = "dram0", "dram1",
					    "shrdram2";
			};

			icssg1_cfg: cfg@b126000 {
				compatible = "syscon";
				reg = <0xb126000 0x200>;
			};

			icssg1_coreclk_mux: coreclk_mux {
				#clock-cells = <0>;
				clocks = <&k3_clks 63 19>, /* icssg1_core_clk */
					 <&k3_clks 63 3>;  /* icssg1_iclk */
				assigned-clocks = <&icssg1_coreclk_mux>;
				assigned-clock-parents = <&k3_clks 63 3>;
			};

			icssg1_iepclk_mux: iepclk_mux {
				#clock-cells = <0>;
				clocks = <&k3_clks 63 10>,	/* icssg1_iep_clk */
					 <&icssg1_coreclk_mux>;	/* core_clk */
				assigned-clocks = <&icssg1_iepclk_mux>;
				assigned-clock-parents = <&icssg1_coreclk_mux>;
			};

			icssg1_iep0: iep@b12e000 {
				compatible = "syscon";
				reg = <0xb12e000 0x1000>;
				clocks = <&icssg1_iepclk_mux>;
			};

			icssg1_iep1: iep@b12f000 {
				compatible = "syscon";
				reg = <0xb12f000 0x1000>;
				clocks = <&icssg1_iepclk_mux>;
			};

			icssg1_mii_rt: mii-rt@b132000 {
				compatible = "syscon";
				reg = <0xb132000 0x100>;
			};

			icssg1_mii_g_rt: mii-g-rt@b133000 {
				compatible = "syscon";
				reg = <0xb133000 0x1000>;
			};

			icssg1_intc: interrupt-controller@b120000 {
				compatible = "ti,am654-icssg-intc";
				reg = <0xb120000 0x2000>;
				interrupt-controller;
				#interrupt-cells = <1>;
			};

			pru1_0: pru@b134000 {
				compatible = "ti,am654-pru";
				reg = <0xb134000 0x4000>,
				      <0xb122000 0x100>,
				      <0xb122400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-pru1_0-fw";
				interrupt-parent = <&icssg1_intc>;
				interrupts = <16>, <17>;
				interrupt-names = "vring", "kick";
			};

			rtu1_0: rtu@b104000 {
				compatible = "ti,am654-rtu";
				reg = <0xb104000 0x2000>,
				      <0xb123000 0x100>,
				      <0xb123400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-rtu1_0-fw";
				interrupt-parent = <&icssg1_intc>;
				interrupts = <20>, <21>;
				interrupt-names = "vring", "kick";
			};

			pru1_1: pru@b138000 {
				compatible = "ti,am654-pru";
				reg = <0xb138000 0x4000>,
				      <0xb124000 0x100>,
				      <0xb124400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-pru1_1-fw";
				interrupt-parent = <&icssg1_intc>;
				interrupts = <18>, <19>;
				interrupt-names = "vring", "kick";
			};

			rtu1_1: rtu@b106000 {
				compatible = "ti,am654-rtu";
				reg = <0xb106000 0x2000>,
				      <0xb123800 0x100>,
				      <0xb123c00 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-rtu1_1-fw";
				interrupt-parent = <&icssg1_intc>;
				interrupts = <22>, <23>;
				interrupt-names = "vring", "kick";
			};

			icssg1_mdio: mdio@b132400 {
				compatible = "ti,davinci_mdio";
				reg = <0xb132400 0x100>;
				clocks = <&k3_clks 63 3>;
				clock-names = "fck";
				#address-cells = <1>;
				#size-cells = <0>;
				bus_freq = <1000000>;
				status = "disabled";
			};
		};
	};

	icssg_soc_bus2: pruss-soc-bus@b226004 {
		compatible = "ti,am654-icssg-soc-bus";
		reg = <0x00 0x0b226004 0x00 0x4>;
		power-domains = <&k3_pds 64>;
		#address-cells = <1>;
		#size-cells = <1>;
		ranges = <0x0b200000 0x00 0x0b200000 0x100000>;
		dma-ranges;

		icssg2: icssg@b200000 {
			compatible = "ti,am654-icssg";
			reg = <0xb200000 0x80000>;
			interrupts = <GIC_SPI 270 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 271 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 272 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 273 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 274 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 275 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 276 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 277 IRQ_TYPE_LEVEL_HIGH>;
			interrupt-names = "host2", "host3", "host4",
					  "host5", "host6", "host7",
					  "host8", "host9";
			#address-cells = <1>;
			#size-cells = <1>;
			ranges;
			dma-ranges;

			icssg2_mem: memories@b200000 {
				reg = <0xb200000 0x2000>,
				      <0xb202000 0x2000>,
				      <0xb210000 0x10000>;
				reg-names = "dram0", "dram1",
					    "shrdram2";
			};

			icssg2_cfg: cfg@b226000 {
				compatible = "syscon";
				reg = <0xb226000 0x200>;
			};

			icssg2_coreclk_mux: coreclk_mux {
				#clock-cells = <0>;
				clocks = <&k3_clks 64 19>, /* icssg1_core_clk */
					 <&k3_clks 64 3>;  /* icssg1_iclk */
				assigned-clocks = <&icssg2_coreclk_mux>;
				assigned-clock-parents = <&k3_clks 64 3>;
			};

			icssg2_iepclk_mux: iepclk_mux {
				#clock-cells = <0>;
				clocks = <&k3_clks 64 10>,	/* icssg1_iep_clk */
					 <&icssg2_coreclk_mux>;	/* core_clk */
				assigned-clocks = <&icssg2_iepclk_mux>;
				assigned-clock-parents = <&icssg2_coreclk_mux>;
			};

			icssg2_iep0: iep@b22e000 {
				compatible = "syscon";
				reg = <0xb22e000 0x1000>;
				clocks = <&icssg2_iepclk_mux>;
			};

			icssg2_iep1: iep@b22f000 {
				compatible = "syscon";
				reg = <0xb22f000 0x1000>;
				clocks = <&icssg2_iepclk_mux>;
			};

			icssg2_mii_rt: mii-rt@b232000 {
				compatible = "syscon";
				reg = <0xb232000 0x100>;
			};

			icssg2_mii_g_rt: mii-g-rt@b233000 {
				compatible = "syscon";
				reg = <0xb233000 0x1000>;
			};

			icssg2_intc: interrupt-controller@b220000 {
				compatible = "ti,am654-icssg-intc";
				reg = <0xb220000 0x2000>;
				interrupt-controller;
				#interrupt-cells = <1>;
			};

			pru2_0: pru@b234000 {
				compatible = "ti,am654-pru";
				reg = <0xb234000 0x4000>,
				      <0xb222000 0x100>,
				      <0xb222400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-pru2_0-fw";
				interrupt-parent = <&icssg2_intc>;
				interrupts = <16>, <17>;
				interrupt-names = "vring", "kick";
			};

			rtu2_0: rtu@b204000 {
				compatible = "ti,am654-rtu";
				reg = <0xb204000 0x2000>,
				      <0xb223000 0x100>,
				      <0xb223400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-rtu2_0-fw";
				interrupt-parent = <&icssg2_intc>;
				interrupts = <20>, <21>;
				interrupt-names = "vring", "kick";
			};

			pru2_1: pru@b238000 {
				compatible = "ti,am654-pru";
				reg = <0xb238000 0x4000>,
				      <0xb224000 0x100>,
				      <0xb224400 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-pru2_1-fw";
				interrupt-parent = <&icssg2_intc>;
				interrupts = <18>, <19>;
				interrupt-names = "vring", "kick";
			};

			rtu2_1: rtu@b206000 {
				compatible = "ti,am654-rtu";
				reg = <0xb206000 0x2000>,
				      <0xb223800 0x100>,
				      <0xb223c00 0x100>;
				reg-names = "iram", "control", "debug";
				firmware-name = "am65x-rtu2_1-fw";
				interrupt-parent = <&icssg2_intc>;
				interrupts = <22>, <23>;
				interrupt-names = "vring", "kick";
			};

			icssg2_mdio: mdio@b232400 {
				compatible = "ti,davinci_mdio";
				reg = <0xb232400 0x100>;
				clocks = <&k3_clks 64 3>;
				clock-names = "fck";
				#address-cells = <1>;
				#size-cells = <0>;
				bus_freq = <1000000>;
				status = "disabled";
			};
		};
	};
};
