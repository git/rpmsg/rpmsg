// SPDX-License-Identifier: GPL-2.0
/*
 * Device Tree Source for AM6 SoC Family MCU Domain peripherals
 *
 * Copyright (C) 2016-2018 Texas Instruments Incorporated - http://www.ti.com/
 */

&cbass_mcu {
	mcu_uart0: serial@40a00000 {
		compatible = "ti,am654-uart";
			reg = <0x00 0x40a00000 0x00 0x100>;
			reg-shift = <2>;
			reg-io-width = <4>;
			interrupts = <GIC_SPI 565 IRQ_TYPE_LEVEL_HIGH>;
			clock-frequency = <96000000>;
			current-speed = <115200>;
	};

	mcu_ram: mcu-ram@41c00000 {
		compatible = "mmio-sram";
		reg = <0x00 0x41c00000 0x00 0x80000>;
		ranges = <0x0 0x00 0x41c00000 0x80000>;
		#address-cells = <1>;
		#size-cells = <1>;

		mcu_r5fss0_core0_sram: r5f-sram@0 {
			reg = <0x0 0x40000>;
		};
	};

	mcu_i2c0: i2c@40b00000 {
		compatible = "ti,am654-i2c", "ti,omap4-i2c";
		reg = <0x0 0x40b00000 0x0 0x100>;
		interrupts = <GIC_SPI 564 IRQ_TYPE_LEVEL_HIGH>;
		#address-cells = <1>;
		#size-cells = <0>;
		clock-names = "fck";
		clocks = <&k3_clks 114 1>;
		power-domains = <&k3_pds 114>;
	};

	mcu_navss: mcu_navss {
		compatible = "simple-bus";
		#address-cells = <2>;
		#size-cells = <2>;
		dma-coherent;
		dma-ranges;
		ranges;

		ti,sci-dev-id = <119>;

		mcu_ringacc: ringacc@2b800000 {
			compatible = "ti,am654-navss-ringacc";
			reg =	<0x0 0x2b800000 0x0 0x400000>,
				<0x0 0x2b000000 0x0 0x400000>,
				<0x0 0x28590000 0x0 0x100>,
				<0x0 0x2a500000 0x0 0x40000>;
			reg-names = "rt", "fifos", "proxy_gcfg", "proxy_target";
			ti,num-rings = <286>;
			ti,gp-rings = <96 255>; /* start, cnt */
			ti,dma-ring-reset-quirk;
			ti,sci = <&dmsc>;
			ti,sci-dev-id = <195>;
			interrupt-parent = <&main_udmass_inta>;
		};

		mcu_udmap: udmap@31150000 {
			compatible = "ti,am654-navss-mcu-udmap";
			reg =	<0x0 0x285c0000 0x0 0x100>,
				<0x0 0x2a800000 0x0 0x40000>,
				<0x0 0x2aa00000 0x0 0x40000>;
			reg-names = "gcfg", "rchanrt", "tchanrt";
			#dma-cells = <3>;

			ti,ringacc = <&mcu_ringacc>;
			ti,psil-base = <0x6000>;

			ti,sci = <&dmsc>;
			ti,sci-dev-id = <194>;

			ti,sci-rm-range-tchan = <0x1>, /* TX_HCHAN */
						<0x2>; /* TX_CHAN */
			ti,sci-rm-range-rchan = <0x3>, /* RX_HCHAN */
						<0x4>; /* RX_CHAN */
			ti,sci-rm-range-rflow = <0x5>; /* GP RFLOW */

			interrupt-parent = <&main_udmass_inta>;
		};
	};

	mcu_pdma0: pdma@40710000 {
		compatible = "ti,am654-pdma";
		reg = <0x0 0x40710000 0x0 0x400>;
		reg-names = "eccaggr_cfg";

		ti,psil-base = <0x7100>;

		/* ti,psil-config0-3 */
		UDMA_PDMA_TR_XY(0);
		UDMA_PDMA_TR_XY(1);
		UDMA_PDMA_TR_XY(2);
		UDMA_PDMA_TR_XY(3);
	};

	mcu_pdma1: pdma@40711000 {
		compatible = "ti,am654-pdma";
		reg = <0x0 0x40711000 0x0 0x400>;
		reg-names = "eccaggr_cfg";

		ti,psil-base = <0x7200>;

		/* ti,psil-config18 */
		UDMA_PDMA_PKT_XY(18);
	};

	mcu_r5fss0: r5fss@41000000 {
		compatible = "ti,am654-r5fss";
		lockstep-mode = <1>;
		#address-cells = <1>;
		#size-cells = <1>;
		ranges = <0x41000000 0x00 0x41000000 0x20000>,
			 <0x41400000 0x00 0x41400000 0x20000>;
		power-domains = <&k3_pds 129>;

		mcu_r5fss0_core0: r5f@41000000 {
			compatible = "ti,am654-r5f";
			reg = <0x41000000 0x00008000>,
			      <0x41010000 0x00008000>;
			reg-names = "atcm", "btcm";
			ti,sci = <&dmsc>;
			ti,sci-dev-id = <159>;
			ti,sci-proc-ids = <0x01 0xFF>;
			resets = <&k3_reset 159 1>;
			atcm-enable = <1>;
			btcm-enable = <1>;
			loczrama = <1>;
			mboxes = <&mailbox0_cluster0 &mbox_mcu_r5fss0_core0>;
			sram = <&mcu_r5fss0_core0_sram>;
		};

		mcu_r5fss0_core1: r5f@41400000 {
			compatible = "ti,am654-r5f";
			reg = <0x41400000 0x00008000>,
			      <0x41410000 0x00008000>;
			reg-names = "atcm", "btcm";
			ti,sci = <&dmsc>;
			ti,sci-dev-id = <245>;
			ti,sci-proc-ids = <0x02 0xFF>;
			resets = <&k3_reset 245 1>;
			atcm-enable = <1>;
			btcm-enable = <1>;
			loczrama = <1>;
			mboxes = <&mailbox0_cluster1 &mbox_mcu_r5fss0_core1>;
		};
	};
};
